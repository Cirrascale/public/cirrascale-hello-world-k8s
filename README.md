# Cirrascale Hello World K8s
<Maintainer: will.hanau@cirrascale.com>

The goal of this repo is to provide beginner kubernetes examples geared towards AI workloads.

You will learn how to run gpu jobs on kubernetes, run and access common AI web tools, and

how to transfer data to a kubernetes cluster's storage.

## Quick start
Follow the "Hello_World_K8s/Hello_World_Kubernetes.pdf" that is geared towards your OS to get started.

## Directories

### Hello_World_K8s
This directory has the cirrascale created kubernetes guides.

### RN50v1.5
From the NVIDIA/DeepLearningExamples github.

https://github.com/NVIDIA/DeepLearningExamples/tree/master/TensorFlow/Classification/RN50v1.5

### Imagenet
A script to download a very small subset of Imagenet 10k transformed to Tfrecords.

Citation:

@article{ILSVRC15,

Author = {Olga Russakovsky and Jia Deng and Hao Su and Jonathan Krause and Sanjeev Satheesh and Sean Ma and Zhiheng Huang and Andrej

Karpathy and Aditya Khosla and Michael Bernstein and Alexander C. Berg and Li Fei-Fei},

Title = {{ImageNet Large Scale Visual Recognition Challenge}},

Year = {2015},

journal   = {International Journal of Computer Vision (IJCV)},

doi = {10.1007/s11263-015-0816-y},

volume={115},

number={3},

pages={211-252}
}

### kubernetes
Cirrascale crafted yaml files that use open source containers from tensorflow and nvidia.

Tensorflow docker image repo: https://hub.docker.com/r/tensorflow/tensorflow

Nvidia docker image repo: https://ngc.nvidia.com/catalog/containers
