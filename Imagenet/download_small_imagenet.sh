#!/usr/bin/bash
# Description: Downloads a tar of ImageNet data to where this script is,
# regardless of what directory it is ran from. Then uncompresses the tar to this
# script's directory.

download_url_prefix="https://storage.googleapis.com/small_training_data/"
object="Small_Imagenet.tar.gz"
executing_filename=$(basename ${BASH_SOURCE})
download_path=${0%"${executing_filename}"}
extract_path=${download_path%"Imagenet/"}

# install wget
apt-get update && apt-get install wget -y

# download
wget -P ${download_path} ${download_url_prefix}${object}

# extract files from tar
tar -xzf ${download_path}/${object} --directory ${extract_path}

# delete tar file
rm ${download_path}${object}
